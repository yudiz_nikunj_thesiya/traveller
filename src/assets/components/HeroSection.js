import React from "react";
import "../styles/herosection.scss";

const HeroSection = () => {
	return (
		<div className="herosection">
			<div className="herosection__heading">
				JOURNEY TO
				<br />
				EXPLORE WORLD
			</div>
			<span className="herosection__desc">
				Ac mi duis mollis. Sapiente? Scelerisque quae, penatibus? Suscipit class
				corporis nostra rem quos voluptatibus habitant? Fames, vivamus minim
				nemo enim, gravida lobortis quasi, eum.
			</span>
			<div className="herosection__btns">
				<button className="herosection__btns-1">LEARN MORE</button>
				<button className="herosection__btns-2">BOOK NOW</button>
			</div>
		</div>
	);
};

export default HeroSection;
