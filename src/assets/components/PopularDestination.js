import React from "react";
import DestinationCard from "./DestinationCard";
import "../styles/populardestination.scss";
import Img1 from "../images/statue.jpg";
import Img2 from "../images/concretehouse.jpg";
import Img3 from "../images/dubai.png";
import Img4 from "../images/banner.jpg";

const PopularDestination = () => {
	return (
		<div className="populardestination">
			<span className="populardestination__label">UNDERCOVER PLACE</span>
			<span className="populardestination__heading">POPULAR DESTINATION</span>
			<span className="populardestination__desc">
				Fusce hic augue velit wisi quibusdam pariatur, iusto primis, nec nemo,
				rutrum. Vestibulum cumque laudantium. Sit ornare mollitia tenetur,
				aptent.
			</span>
			<div className="card-container">
				<DestinationCard
					img={Img1}
					label="ITALY"
					heading="CONCRETE STATUE"
					desc="Fusce hic augue velit wisi ips quibusdam pariatur, iusto."
				/>
				<DestinationCard
					img={Img3}
					label="DUBAI"
					heading="BURJ KHALIFA"
					desc="Fusce hic augue velit wisi ips quibusdam pariatur, iusto."
				/>
				<DestinationCard
					img={Img2}
					label="ITALY"
					heading="CONCRETE HOUSE"
					desc="Fusce hic augue velit wisi ips quibusdam pariatur, iusto."
				/>
				<DestinationCard
					img={Img3}
					label="DUBAI"
					heading="BURJ AL ARAB"
					desc="Fusce hic augue velit wisi ips quibusdam pariatur, iusto."
				/>
				<DestinationCard
					img={Img4}
					label="INDONESIA"
					heading="BALI"
					desc="Fusce hic augue velit wisi ips quibusdam pariatur, iusto."
				/>
			</div>
		</div>
	);
};

export default PopularDestination;
