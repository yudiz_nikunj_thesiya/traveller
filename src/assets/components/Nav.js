import React, { useState } from "react";
import logo from "../images/logo.png";
import "../styles/nav.scss";
import { RiMenu3Line } from "react-icons/ri";
import "../styles/verticalnav.scss";
import { MdOutlineClose } from "react-icons/md";

const Nav = () => {
	const [mobileNav, setMobileNav] = useState(false);
	return (
		<div className="container">
			<div className="nav">
				<img src={logo} alt="logo" className="nav__logo" />
				<div className="nav__menu">
					<span>HOME</span>
					<span>ABOUT US</span>
					<span>DESTINATION</span>
					<span>PACKAGES</span>
					<span>CONTACT</span>
				</div>
				<div className="nav__btns">
					<button className="nav__btns--btn-2">Book Now</button>
					<button
						className="nav__btns--btn-1"
						onClick={() => setMobileNav(true)}
					>
						<RiMenu3Line />
					</button>
				</div>
			</div>
			{mobileNav && (
				<div className="verticalnav">
					<div className="verticalnav__header">
						<img src={logo} alt="logo" className="verticalnav__header--logo" />
						<button
							className="verticalnav__header--btn"
							onClick={() => setMobileNav(false)}
						>
							<MdOutlineClose />
						</button>
					</div>
					<div className="verticalnav__menu">
						<span onClick={() => setMobileNav(false)}>HOME</span>
						<span onClick={() => setMobileNav(false)}>ABOUT US</span>
						<span onClick={() => setMobileNav(false)}>DESTINATION</span>
						<span onClick={() => setMobileNav(false)}>PACKAGES</span>
						<span onClick={() => setMobileNav(false)}>CONTACT</span>
					</div>
				</div>
			)}
		</div>
	);
};

export default Nav;
