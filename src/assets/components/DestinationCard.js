import React from "react";
import "../styles/populardestination.scss";
import img from "../images/concretehouse.jpg";
import { BsStarFill } from "react-icons/bs";

const DestinationCard = ({ img, label, heading, desc }) => {
	return (
		<div className="card">
			<img src={img} alt="house" className="card__image" />
			<div className="card__details">
				<span className="card__details--label">{label}</span>
				<div className="card__details--star">
					<BsStarFill />
					<BsStarFill />
					<BsStarFill />
					<BsStarFill />
					<BsStarFill />
				</div>

				<span className="card__details--heading">{heading}</span>
				<span className="card__details--desc">{desc}</span>
			</div>
		</div>
	);
};

export default DestinationCard;
