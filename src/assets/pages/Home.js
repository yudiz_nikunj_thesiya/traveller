import React from "react";
import HeroSection from "../components/HeroSection";
import Inquiry from "../components/Inquiry";
import Nav from "../components/Nav";
import PopularDestination from "../components/PopularDestination";
import "../styles/home.scss";

const Home = () => {
	return (
		<div className="home">
			<Nav />
			<HeroSection />
			<Inquiry />
			<PopularDestination />
		</div>
	);
};

export default Home;
